//
// GORDONSTREET (gordonstreet)
//
// Self Executing Anonymous Function:
// -Enables use of private and public properties/methods
// -Also protects jQuery $ and undefined from conflicts
//
(function( gordonstreet, $, undefined ) {


	function scrollToTop() {
		$('#gordonChat').scrollTop(0);
	}

	// chat functions
	gordonstreet.sendPublicMessage = function(message) {
		var senderApt = $.cookie('apartment');
		firebase.database().ref('/Gordon-Street').push({
			message: message,
			sender: senderApt
		});
	}

	gordonstreet.loadPublicMessages = function() {
		var database = firebase.database().ref('/Gordon-Street');

		database.once('value', function(snapshot) {
			var loadedMessages = snapshot.val();

			$.each(loadedMessages, function(key, value) {
				var messageText = value.message;
				var messageSender = value.sender;
				
				$('#gordonChat').prepend('\
					<div class="chat-message cf">\
						<div class="chat-message-left">\
							<h2 class="apt-number">Apt. #' + messageSender + '</h2>\
						</div>\
						<div class="chat-message-right">\
							<p class="chat-message-content">' + messageText + '</p>\
						</div>\
					</div>\
				')
			});
		})
	}

	gordonstreet.loadNewPublicMessages = function() {
		var database = firebase.database().ref('/Gordon-Street');
		database.limitToLast(1).on('child_added', function(snapshot) {
			var value = snapshot.val();
			var messageText = value.message;
			var messageSender = value.sender;

			$('#gordonChat').prepend('\
				<div class="chat-message cf">\
					<div class="chat-message-left">\
						<h2 class="apt-number">Apt. #' + messageSender + '</h2>\
					</div>\
					<div class="chat-message-right">\
						<p class="chat-message-content">' + messageText + '</p>\
					</div>\
				</div>\
			')

			// scroll to bottom

			scrollToTop();
		});
	};


	gordonstreet.cookieBaker = function() {
		var aptNumber = $('.sign-up input').val();
		console.log(aptNumber);
		$.cookie('apartment', aptNumber);
	};
		


	


	//INIT
	gordonstreet.init = function() {

		// Put document ready code here!
		console.log('Document Ready!');

		var cookieId = $.cookie('apartment');
		if ( cookieId ) {
			$('body').addClass('signed-in');
		}


		gordonstreet.loadPublicMessages();
		gordonstreet.loadNewPublicMessages();


		// show chat input
		$('body').on('click', '.show-chat-button', function() {
			$('body').toggleClass('send-message-open');
		});
		$('body').on('click', '.main-container', function() {
			$('body').removeClass('send-message-open');
		})


		// send public message
		$('body').on('click', '#sendMessage', function() {
			var messageText = $('.chat-input-container textarea').val();
			gordonstreet.sendPublicMessage(messageText);
			$('body').removeClass('send-message-open');
			$('.chat-input-container textarea').val('');
		});

		// log in
		$('body').on('click', '#signIn', function() {
			$('body').addClass('signed-in');
			gordonstreet.cookieBaker();
		});

	};


	// -----------------------------------------
	// DOCUMENT READY
	//
	$(document).ready(function() { gordonstreet.init(); });

}(window.gordonstreet = window.gordonstreet || {}, jQuery));